
package kr.ac.kopo.dlrkd.test4

import android.os.Handler

class ServiceThread(internal var handler: Handler) : Thread() {
    internal var isRun = true


    fun stopForever() {
        synchronized(this) {
            this.isRun = false
        }
    }

    override fun run() {
        //반복적으로 수행할 작업을 한다.
        while (isRun) {
            handler.sendEmptyMessage(0)
            try {
                Thread.sleep(5000)
            } catch (e: Exception) {
            }

        }
    }
}

