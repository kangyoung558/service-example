package kr.ac.kopo.dlrkd.test4

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import java.util.*

class MyService : Service() {
    private  lateinit var  handler: myServiceHandler
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable
    internal var thread: ServiceThread? = null


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        mHandler = Handler()
        mRunnable = Runnable { showRandomNumber() }
        mHandler.postDelayed(mRunnable, 5000)
        handler = myServiceHandler()
        thread = ServiceThread(handler)
        thread!!.start()
        return Service.START_STICKY
    }

    //서비스가 종료될 때 할 작업

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(applicationContext,"Service destroyed.", Toast.LENGTH_SHORT).show()
        mHandler.removeCallbacks(mRunnable)
        thread!!.stopForever()
        thread = null//쓰레기 값을 만들어서 빠르게 회수하라고 null을 넣어줌.
    }
    private fun showRandomNumber() {
        val rand = Random()
        val number = rand.nextInt(100)
        Toast.makeText(applicationContext,"Random Number : $number", Toast.LENGTH_SHORT).show()
        mHandler.postDelayed(mRunnable, 60000)
    }

    internal inner class myServiceHandler : Handler() {
        override fun handleMessage(msg: android.os.Message) {
            val intent = Intent(this@MyService, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this@MyService, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N_MR1) {
                /**
                 * 누가버전 이하 노티처리
                 */
                Toast.makeText(applicationContext, "누가버전이하", Toast.LENGTH_SHORT).show()

                //                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                val bitmapDrawable = resources.getDrawable(R.mipmap.ic_launcher) as BitmapDrawable
                val bitmap = bitmapDrawable.bitmap
                val builder = NotificationCompat.Builder(applicationContext).setLargeIcon(bitmap)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setWhen(System.currentTimeMillis()).setShowWhen(true).setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setContentTitle("노티테스트!!")
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setFullScreenIntent(pendingIntent, true)
                    .setContentIntent(pendingIntent)

                val notificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(0, builder.build())

            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Toast.makeText(applicationContext, "오레오이상", Toast.LENGTH_SHORT).show()
                /**
                 * 오레오 이상 노티처리
                 */
                //BitmapDrawable bitmapDrawable = (BitmapDrawable)getResources().getDrawable(R.mipmap.ic_launcher);
                //Bitmap bitmap = bitmapDrawable.getBitmap();
                /**
                 * 오레오 버전부터 노티를 처리하려면 채널이 존재해야합니다.
                 */

                val importance = NotificationManager.IMPORTANCE_HIGH
                val Noti_Channel_ID = "Noti"
                val Noti_Channel_Group_ID = "Noti_Group"

                val notificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val notificationChannel =
                    NotificationChannel(Noti_Channel_ID, Noti_Channel_Group_ID, importance)

                //notificationManager.deleteNotificationChannel("testid"); 채널삭제

                /**
                 * 채널이 있는지 체크해서 없을경우 만들고 있으면 채널을 재사용합니다.
                 */
                if (notificationManager.getNotificationChannel(Noti_Channel_ID) != null) {
                    Toast.makeText(applicationContext, "채널이 이미 존재합니다.", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(applicationContext, "채널이 없어서 만듭니다.", Toast.LENGTH_SHORT).show()
                    notificationManager.createNotificationChannel(notificationChannel)
                }

                notificationManager.createNotificationChannel(notificationChannel)
                //Log.e("로그확인","===="+notificationManager.getNotificationChannel("testid1"));
                //notificationManager.getNotificationChannel("testid");


                val builder = NotificationCompat.Builder(applicationContext, Noti_Channel_ID)
                    .setLargeIcon(null)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setWhen(System.currentTimeMillis()).setShowWhen(true).setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setContentTitle("노티테스트!!")
                //   .setContentIntent(pendingIntent);

                // NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(0, builder.build())

            }
        }
    }
}
