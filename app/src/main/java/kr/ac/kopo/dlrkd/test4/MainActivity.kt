package kr.ac.kopo.dlrkd.test4


import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val serviceClass = MyService::class.java
        val intent = Intent(applicationContext, serviceClass)


        button_start.setOnClickListener {
            if (!isServiceRunning(serviceClass)) {
                startService(intent)
                toast("severice 시작")
            } else {
                toast("이미 실행중")
            }
        }

        button_stop.setOnClickListener {
            if (isServiceRunning(serviceClass)) {
                // Stop the service
                stopService(intent)
                toast("Service 끝")
            } else {
                toast("이미 종료됨")
            }
        }
        button_stats.setOnClickListener{
            if (isServiceRunning(serviceClass)) {
                toast("Service가 실행중입니다")
            } else {
                toast("Service가 종료됨")
            }

        }
    }

    private fun isServiceRunning(serviceClass: Class<*>): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        // Loop through the running services
        for (service in activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                // If the service is running then return true
                return true
            }
        }
        return false
    }
}
    fun Context.toast(message:String){
        Toast.makeText(applicationContext,message,Toast.LENGTH_SHORT).show()
}

